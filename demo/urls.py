from softui.settings import BASE_DIR
# from django.contrib.auth import logout
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
import os


urlpatterns = [
    path('signin',views.Signin.as_view()),
    path('signup',views.Signup.as_view()),
    path('logout',views.Logout.as_view()),
    path('autologin',views.AutoLogin.as_view()),
    path('personalinfo',views.personalinfo.as_view()),
    path('companybranddetails',views.companybrand.as_view()),
    path('fileupload',views.fileupload.as_view()),
    path('forecasting',views.forecasting.as_view()),
    path('dashboard',views.dasboard.as_view()),
    path('reports',views.reports.as_view()),
    path('profile',views.profile.as_view()),
    path('mohit/Reports/<str:path>',serve,{
        'document_root' : os.path.join(BASE_DIR,'mohit/Reports')
    })
]