from mohit.settings import *
from mohit.main import main

def process_file(forecast_period,target,lead_time,username,companyname,brand):

    # calling the main.py of mohit's funtions
    try:
        forecast_period = int(forecast_period)
        target = int(target)
        lead_time = int(lead_time)
        try:
            main(
                forecast_period,
                target,
                lead_time,
                username,
                companyname,
                brand
            )
            return True
        except:
            "something went wrong while processing the data"
    except:
        return "values must be integer"

