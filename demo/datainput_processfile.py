import pandas as pd
import time
import os
from datetime import datetime
from .models import sales_data,backorder_data,inventory_data

# for sales data 
def process_sales_data(username,companyname,brand,main_path,path):

    try:
        # reading excel
        df_excel = pd.read_excel(
            '{}\{}'.format(main_path,path)
        )
        # taking dealer column
        dealer_column = df_excel[df_excel.columns[2]]
        dealer_null_index = []

        for i in range(0,len(dealer_column)):
            if type(dealer_column[i]) != str:
                dealer_null_index.append(i)
        
        # dropping null column
        df_excel = df_excel.drop(
            labels=dealer_null_index
        )

        # taking header column for column index
        colums = list(df_excel.iloc[0])
        df_excel.columns = colums

        df_excel = df_excel.drop(
            index=df_excel.index[0]
        ).reset_index(
            drop=True
        )
        try:
            unique_partno = df_excel['Part No'].unique()
            min_date = df_excel['Date'].min()
            max_date = df_excel['Date'].max()
        except:
            return False
        
        date_range = pd.date_range(min_date,max_date)
        col = ['Part No','Part Name','Total Qty']
        for values in date_range:
            col.append(str(values.date()))
        index = list(range(len(unique_partno)))

        # creating new empty dataframe
        new_df = pd.DataFrame(
            columns=col,
            index=index
        )
        exclude = []
        for i in range(0,len(unique_partno)):
            if unique_partno[i] not in exclude:
                try:
                    new_df['Part No'][i] = unique_partno[i]

                    temp_df = df_excel.loc[df_excel['Part No'] == unique_partno[i]]
                    new_df['Part Name'][i] = temp_df['Part Name'].reset_index(drop=True)[0]

                    part_date = temp_df['Date'].reset_index(drop=True)
                    part_qty = temp_df['Qty'].reset_index(drop=True)

                    count = 0
                    for j in range(0,len(part_date)):
                        new_df[str(part_date[j].date())][i] = part_qty[j]
                        count += int(part_qty[j])
                    new_df['Total Qty'][i] = count
                except:
                    return False
            else:
                exclude.append(unique_partno[i])
        new_df = new_df.fillna(0)
        try:
            # previouly we're saving the file in media\processedfile
            # filename = path.split('.')[0]
            # new_df.to_excel('{}\{}\{}.{}'.format(main_path,'processedfile',filename,'xlsx'))
            date_col = new_df.columns[3:]
            partno = new_df.iloc[:,0]
            print(datetime.now())
            # print(date_col)
            for i in range(0,len(partno)):
                # temp = []
                for val in date_col:
                    
                    sales_data.objects.create(
                        username=username,
                        companyname=companyname,
                        brand=brand,
                        partno=new_df.iloc[i][0],
                        partname=new_df.iloc[i][1],
                        date=datetime.strptime(val,'%Y-%m-%d'),
                        qty=new_df.iloc[i][val]
                    )
                    break
            print(datetime.now())
            os.remove('{}\{}'.format(main_path,path))
            return True
        except:
            return False
    except:
        return "file not found"


# for invetory data
def process_backorder_data(username,companyname,brand,main_path,path):
        try:
            # reading excel
            df_excel = pd.read_excel(
                '{}\{}'.format(main_path,path)
            )
            # taking dealer column
            # not dealer column here it's part no column
            dealer_column = df_excel[df_excel.columns[1]]
            dealer_null_index = []

            for i in range(0,len(dealer_column)):
                if type(dealer_column[i]) != str:
                    dealer_null_index.append(i)
            
            # dropping null column
            df_excel = df_excel.drop(
                labels=dealer_null_index
            )

            # taking header column for column index
            colums = list(df_excel.iloc[0])
            df_excel.columns = colums

            df_excel = df_excel.drop(
                index=df_excel.index[0]
            ).reset_index(
                drop=True
            )
            defined_col_name = ("part no","back order")
            new_col_name = []
            for c_name in df_excel.columns:
                if c_name.lower() in defined_col_name:
                    new_col_name.append(c_name)
            if len(new_col_name) == 2:
                for i in range(len(df_excel)):
                    backorder_data.objects.create(
                        username=username,
                        companyname=companyname,
                        brand=brand,
                        partno=df_excel.iloc[i][new_col_name[0]],
                        qty=df_excel.iloc[i][new_col_name[1]],
                        date=datetime.now().date()
                    )
            os.remove('{}\{}'.format(main_path,path))
            return True
        except:
            return "file not found"


# for backorder data
def process_inventory_data(username,companyname,brand,main_path,path):
        try:
            # reading excel
            df_excel = pd.read_excel(
                '{}\{}'.format(main_path,path)
            )
            # taking dealer column
            # not dealer column here it's part no column
            dealer_column = df_excel[df_excel.columns[3]]
            dealer_null_index = []

            for i in range(0,len(dealer_column)):
                if type(dealer_column[i]) != str:
                    dealer_null_index.append(i)
            
            # dropping null column
            df_excel = df_excel.drop(
                labels=dealer_null_index
            )

            # taking header column for column index
            colums = list(df_excel.iloc[0])
            df_excel.columns = colums

            df_excel = df_excel.drop(
                index=df_excel.index[0]
            ).reset_index(
                drop=True
            )
            defined_col_name = ("part no","inventory","price")
            new_col_name = []
            for c_name in df_excel.columns:
                if c_name.lower() in defined_col_name:
                    new_col_name.append(c_name)
            if len(new_col_name) == 3:
                for i in range(len(df_excel)):
                    inventory_data.objects.create(
                        username=username,
                        companyname=companyname,
                        brand=brand,
                        partno=df_excel.iloc[i][new_col_name[0]],
                        qty=df_excel.iloc[i][new_col_name[1]],
                        rate=df_excel.iloc[i][new_col_name[2]],
                        date=datetime.now().date(),
                    )
            os.remove('{}\{}'.format(main_path,path))
            return True
        except:
            return "file not found"


# entry point of other function
def processdata_main(username,companyname,brand,datatype,main_path,path):
    if datatype == "sales":
        return process_sales_data(username,companyname,brand,main_path,path)
    elif datatype == "inventory":
        return process_inventory_data(username,companyname,brand,main_path,path)
    elif datatype == "backorder":
        return process_backorder_data(username,companyname,brand,main_path,path)