from .models import PersonalInfo, Company, Brand


def personalinfo_store(user,data):
    try:
        phonenumber = data['phonenumber']
        city = data["city"]
        category = data['category']
        partsinventory = data['partsinventory']
        company = data.getlist('company[]')
        brand = data.getlist("brand[]")
        # print(company)
        # print(brand)
    except:
        return False

    if phonenumber and city and category and company and brand and partsinventory:
        if len(company) == len(brand):
            PersonalInfo.objects.create(
                user=user,
                phonenumber=phonenumber,
                partsinventory=partsinventory,
            )
            for i in range(0,len(company)):
                company_obj = Company.objects.create(
                    user=user,
                    companyname=company[i],
                    city=city,
                    category=category
                )
                Brand.objects.create(
                    companyname=company_obj,
                    brand=brand[i]
                )
            return True
        else:
            return "company brand empty"
    else:
        return "blank fields"
