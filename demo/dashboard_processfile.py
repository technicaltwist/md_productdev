import pandas as pd
import os
from softui.settings import BASE_DIR
from mohit.functions import select_specific_file


from .models import sales_data
from django.db.models import Max,Min



def get_process_data(username,data):
    companyname = data['companyname'].lower()
    brand = data['brand'].lower()
    # processed folder
    process_folder_path = os.path.join(
        BASE_DIR, 'mohit/processed'
    )
    main_data = {}
    all_files = os.listdir(process_folder_path)

    latest_file = select_specific_file(all_files,
    username,
    companyname,
    brand)
    if latest_file:
        df = pd.read_excel(
            '{}\{}'.format(process_folder_path,latest_file)
        )

        # monthaly sale trend
        def for_monthly_sale():
            month = [
                'january',
                'february',
                'march', 
                'april', 
                'june', 
                'july', 
                'august', 
                'september', 
                'october', 
                'november', 
                'december'
            ]
            # months in excel file
            all_months = []
            all_months_data = []
            for col in df.columns:
                for j in month:
                    if col.lower() in j.lower():
                        all_months.append(col)
            for month in all_months:
                count = 0
                for value in df[month]:
                    count += value
                all_months_data.append(count)
            return {
                "months" : all_months,
                "count" : all_months_data
            }
        
        # for abcd breakup
        def for_abc_breakup():
            abc = df['ABC']
            a,b,c,d,total = 0,0,0,0,0.0
            for values in abc:
                if values.lower() == 'a':
                    a += 1
                elif values.lower() == 'b':
                    b += 1
                elif values.lower() == 'c':
                    c += 1
                elif values.lower() == 'd':
                    d += 1
                total +=1
            per_a = (100*a)/total
            per_b = (100*b)/total
            per_c = (100*c)/total
            per_d = (100*d)/total
            return [
                {
                    "label": "A",
                    "data": per_a,
                },
                {
                    "label": "B",
                    "data": per_b,
                },
                {
                    "label": "C",
                    "data": per_c,
                },
                {
                    "label": "D",
                    "data": per_d
                }
            ]
        
        # for fmsz break up
        def for_fms_breakup():
            fms = df['FMS']
            f,m,s,z,total = 0,0,0,0,0.0
            for values in fms:
                if values.lower() == 'f':
                    f += 1
                elif values.lower() == 'm':
                    m += 1
                elif values.lower() == 's':
                    s += 1
                elif values.lower() == 'z':
                    z += 1
                total +=1
            per_f = (100*f)/total
            per_m = (100*m)/total
            per_s = (100*s)/total
            per_z = (100*z)/total
            return [
                {
                    "label": "F",
                    "data": per_f,
                },
                {
                    "label": "M",
                    "data": per_m,
                },
                {
                    "label": "S",
                    "data": per_s,
                },
                {
                    "label": "Z",
                    "data": per_z
                }
            ]

        # for category breakup
        def for_category_breakup():
            category = df['Category']
            s,e,l,i,total = 0,0,0,0,0.0
            for values in category:
                if values.lower() == 'smooth':
                    s += 1
                elif values.lower() == 'eratic':
                    e += 1
                elif values.lower() == 'lumpy':
                    l += 1
                elif values.lower() == 'intermittent':
                    i += 1
                total +=1
            per_s = (100*s)/total
            per_e = (100*e)/total
            per_l = (100*l)/total
            per_i = (100*i)/total
            return [
                {
                    "label": "Smooth",
                    "data": per_s,
                },
                {
                    "label": "Eratic",
                    "data": per_e,
                },
                {
                    "label": "Lumpy",
                    "data": per_l,
                },
                {
                    "label": "Intermittent",
                    "data": per_i
                }
            ]

        main_data.update({"monthly_sale":for_monthly_sale()})
        main_data.update({"abc_breakup":for_abc_breakup()})
        main_data.update({"fms_breakup":for_fms_breakup()})
        main_data.update({"category_breakup":for_category_breakup()})
        return main_data
    else:
        return False


def create_dataframe_from_database(username,companyname,brand):

    # taking the unique part no for a
    # particular username, companyname and brand
    specific_partno = sales_data.objects.filter(
        username=username
    ).filter(
        companyname=companyname
    ).filter(
        brand=brand
    )
    # taking the unique part no from the whole data
    unique_partno = specific_partno.distinct("partno")

    columns = ['Part No.',"Part Name"]
    months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    # talking the max date and minimum date from the sales data
    max_date = specific_partno.aggregate(Max("date"))['date__max'].date()
    min_date = specific_partno.aggregate(Min("date"))['date__min'].date()
    max_month = max_date.month
    max_year = max_date.year
    min_month = min_date.month
    min_year = max_date.year
    if max_month == min_month and min_year == max_year:
        columns.append(months[max_month-1])
    else:
        if max_year == min_year:
            while max_month >= min_month:
                columns.append(months[min_month-1])
                min_month+=1
        elif max_year > min_year:
            max_month = max_month + ((max_year-min_year)*12)
            while max_month >= min_month:
                if min_month%12 != 0:
                    columns.append(months[min_month%12-1])
                    min_month +=1
                else:
                    columns.append(months[11])
                    min_month +=1
    # columns = ['Part No.',"Part Name",<all months of sales : eg => "mar","apr">,"Inventory","BO","Price"]
    columns += ["Inventory","BO","Price"]
    # creating a dataframe
    # with len(unique_partno) as row
    # and columns as column
    df = pd.DataFrame(
        index=list(range(len(unique_partno))),
        columns=columns
    )
    
    for partno in unique_partno:
        all_deatils_partno = specific_partno.filter(partno=partno)
        for each in all_deatils_partno:
            print(each.date.date().month)
        break

    return True
