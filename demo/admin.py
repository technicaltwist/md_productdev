from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User,PersonalInfo,Company,Brand, sales_data,inventory_data,backorder_data

# Register your models here.

admin.site.register(User,UserAdmin)
admin.site.register(PersonalInfo)
admin.site.register(Company)
admin.site.register(Brand)
admin.site.register(sales_data)
admin.site.register(backorder_data)
admin.site.register(inventory_data)