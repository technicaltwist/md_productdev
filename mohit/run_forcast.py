from . import settings
import pandas as pd
from . import functions as ft
from .forcast import Forcast


def run_forcast(path,file,forcast_days,target_days,process_transit_days,username,companyname,brand):
    sku_list,data,price,inventory,backorder,columns,name_list,category=ft.prep_forcast(path)
    master=[]
    i=0
    # print('loop Started')
    for sku_name in sku_list:
        # print(sku_name)
        Forcast(data,inventory,price,backorder,sku_name,name_list,sku_list,
        category,forcast_days,settings.saftey_stock_days,process_transit_days,period=1
        )
        master.append(Forcast.storage[sku_name].final_output)
        i+=1
        # print(sku_name)
    # print('Loop ended')
    df=pd.DataFrame(master,columns=['Part No.','Part Name','Part Type']+columns+settings.columns_creation)
    df=df.sort_values(by='Total Value',ascending=False)
    df=df.reset_index()
    ft.calculate_abc(df)
    df=df[['Part No.','Part Name','Part Type']+columns+settings.columns_output]
    ft.get_target(
        df,
        username,
        companyname,
        brand,
        target_days
    )
    df.to_excel('{}\{}'.format(settings.processed_path,file),index=False)