#import settings
import pandas as pd
import datetime
import numpy as np
from . import settings
from os import listdir
from os.path import isfile, join

def prep_forcast(file):
    df=pd.read_excel(file)
    df=df.fillna(0)
    skus=df['Part No.'].tolist()
    inventory=df['Inventory'].tolist()
    backorder=df['BO'].tolist()
    price=df['Price'].tolist()
    name=df['Part Name'].tolist()
    category=df['Category'].tolist()
    df=df.drop(['Part No.','Inventory','BO','Price','Part Name','Category'],axis=1)
    columns=list(df.columns)
    data=[]
    for i in df.index:
        l=df.loc[i].tolist()
        data.append(l)
    
    return skus,data,price,inventory,backorder,columns,name,category

# Croston lies here :p

def croston_tsb_forcast(ts,extra_periods=1,alpha=0.4,beta=0.4):
    d = np.array(ts) # Transform the input into a numpy array
    cols = len(d) # Historical period length
    d = np.append(d,[np.nan]*extra_periods) # Append np.nan into the demand array to cover future periods
    
    #level (a), probability(p) and forecast (f)
    a,p,f = np.full((3,cols+extra_periods),np.nan)
    
    # Initialization
    first_occurence = np.argmax(d[:cols]>0)
    a[0] = d[first_occurence]
    p[0] = 1/(1 + first_occurence)
    f[0] = p[0]*a[0]
                 
    # Create all the t+1 forecasts
    for t in range(0,cols): 
        if d[t] > 0:
            a[t+1] = alpha*d[t] + (1-alpha)*a[t] 
            p[t+1] = beta*(1) + (1-beta)*p[t]
        else:
            a[t+1] = a[t]
            p[t+1] = (1-beta)*p[t]       
        f[t+1] = p[t+1]*a[t+1]
        
    #print(math.ceil(1/p[-1]-1)) 
    err=np.sqrt(((d[:-1]-f[:-1])**2).mean())
                  
    return int(f[-1]),err

def croston_forcast(ts,extra_periods=1,alpha=0.4):
    d = np.array(ts) # Transform the input into a numpy array
    cols = len(d) # Historical period length
    d = np.append(d,[np.nan]*extra_periods) # Append np.nan into the demand array to cover future periods
    
    #level (a), periodicity(p) and forecast (f)
    a,p,f = np.full((3,cols+extra_periods),np.nan)
    q = 1 #periods since last demand observation
    
    # Initialization
    first_occurence = np.argmax(d[:cols]>0)
    a[0] = d[first_occurence]
    p[0] = 1 + first_occurence
    f[0] = a[0]/p[0]
    # Create all the t+1 forecasts
    for t in range(0,cols):        
        if d[t] > 0:
            a[t+1] = alpha*d[t] + (1-alpha)*a[t] 
            p[t+1] = alpha*q + (1-alpha)*p[t]
            f[t+1] = a[t+1]/p[t+1]
            q = 1           
        else:
            a[t+1] = a[t]
            p[t+1] = p[t]
            f[t+1] = f[t]
            q += 1

    err=np.sqrt(((d[:-1]-f[:-1])**2).mean())
    
    return int(f[-1]),err
    #return f[-1]

def syntetos_boylan_forcast(ts,extra_periods=1,alpha=0.4):
    d = np.array(ts) # Transform the input into a numpy array
    cols = len(d) # Historical period length
    d = np.append(d,[np.nan]*extra_periods) # Append np.nan into the demand array to cover future periods
    
    #level (a), periodicity(p) and forecast (f)
    a,p,f = np.full((3,cols+extra_periods),np.nan)
    q = 1 #periods since last demand observation
    
    # Initialization
    first_occurence = np.argmax(d[:cols]>0)
    a[0] = d[first_occurence]
    p[0] = 1 + first_occurence
    f[0] = a[0]/p[0]
    # Create all the t+1 forecasts
    for t in range(0,cols):        
        if d[t] > 0:
            a[t+1] = alpha*d[t] + (1-alpha)*a[t] 
            p[t+1] = alpha*q + (1-alpha)*p[t]
            f[t+1] = (1-(alpha/2))*(a[t+1]/p[t+1])
            q = 1           
        else:
            a[t+1] = a[t]
            p[t+1] = p[t]
            f[t+1] = f[t]
            q += 1

    err=np.sqrt(((d[:-1]-f[:-1])**2).mean())
    
    return int(f[-1]),err

def leven_seger_forcast(ts,extra_periods=1,alpha=0.4):
    d = np.array(ts) # Transform the input into a numpy array
    cols = len(d) # Historical period length
    d = np.append(d,[np.nan]*extra_periods) # Append np.nan into the demand array to cover future periods
    
    #level (a), periodicity(p) and forecast (f)
    a,p,f = np.full((3,cols+extra_periods),np.nan)
    q = 1 #periods since last demand observation
    
    # Initialization
    first_occurence = np.argmax(d[:cols]>0)
    a[0] = d[first_occurence]
    p[0] = 1 + first_occurence
    f[0] = a[0]/p[0]
    # Create all the t+1 forecasts
    for t in range(0,cols):        
        if d[t] > 0:
            a[t+1] = alpha*d[t] + (1-alpha)*a[t] 
            p[t+1] = alpha*q + (1-alpha)*p[t]
            f[t+1] = alpha*(a[t+1]/p[t+1])+(1-alpha)*d[t]
            q = 1           
        else:
            a[t+1] = a[t]
            p[t+1] = p[t]
            f[t+1] = f[t]
            q += 1
    
    err=np.sqrt(((d[:-1]-f[:-1])**2).mean())
    
    return int(f[-1]),err

def vinh_forcast(ts,extra_periods=1,alpha=0.4):
    d = np.array(ts) # Transform the input into a numpy array
    cols = len(d) # Historical period length
    d = np.append(d,[np.nan]*extra_periods) # Append np.nan into the demand array to cover future periods
    
    #level (a), periodicity(p) and forecast (f)
    a,p,f = np.full((3,cols+extra_periods),np.nan)
    q = 1 #periods since last demand observation
    
    # Initialization
    first_occurence = np.argmax(d[:cols]>0)
    a[0] = d[first_occurence]
    p[0] = 1 + first_occurence
    f[0] = a[0]/p[0]
    # Create all the t+1 forecasts
    for t in range(0,cols):
        if d[t] > 0 and t>0:
            a[t+1] = alpha*d[t] + (1-alpha)*a[t] + ((1-alpha)**2)*a[t-1]
            p[t+1] = alpha*q + (1-alpha)*p[t] + ((1-alpha)**2)*p[t-1]
            f[t+1] = a[t+1]/p[t+1]
            q = 1
        elif d[t] > 0:
            a[t+1] = alpha*d[t] + (1-alpha)*a[t] 
            p[t+1] = alpha*q + (1-alpha)*p[t]
            f[t+1] = a[t+1]/p[t+1]
            q = 1 
        else:
            a[t+1] = a[t]
            p[t+1] = p[t]
            f[t+1] = f[t]
            q += 1

    err=np.sqrt(((d[:-1]-f[:-1])**2).mean())
    
    return int(f[-1]),err

    # Seasonal is pain in ass :(
    
def seasonal_forcast(data,period,marker):
    if marker=='m':
        cycle=int(len(data)/12)
        #print(data)
        if cycle>=1:
            x=[]
            for i in range(cycle):
                j=i+1
                value = np.average(data[-12*j:-12+period])
                #value = data[-11]+data[-12]+data[-13]
                x.append(value)
            return np.sum(x)/len(x),1
        else:
            return 0,0
    elif marker=='w':
        cycle=int(len(data)/53)
        if cycle>=1:
            x=[]
            for i in range(cycle):
                j=i+1
                value = data[-51*j]+data[-52*j]+data[-53*j]+data[-54*j]+data[-55*j]
                x.append(value)
            return np.sum(x)/4
    else:
        return 0,0
    #value = data[-52]+data[-53]+data[-54]
    #return value/2    
    
# Averages why you are soo smooth :)

def average_forcast(data):
    a3=int(np.sum(data[-3:])/3)
    a6=int(np.sum(data[-6:])/6)
    a9=int(np.sum(data[-9:])/9)
    a12=int(np.sum(data[-12:])/12)
    return [a3,a6,a9,a12]
    #return a3

def train_model(model,data,num=15):
    alphas=np.linspace(0.0,1.0,num)
    err=[]
    res=[]
    if model=='Croston':
        for alpha in alphas:
            r,e=croston_forcast(data,1,alpha)
            res.append(r)
            err.append(e)
        ind=err.index(min(err))
    elif model=='Croston TSB':
        for alpha in alphas:
            r,e=croston_tsb_forcast(data,1,alpha)
            res.append(r)
            err.append(e)
        ind=err.index(min(err))
    elif model=='Syntetos Boylan':
        for alpha in alphas:
            r,e=syntetos_boylan_forcast(data,1,alpha)
            res.append(r)
            err.append(e)
        ind=err.index(min(err))
    elif model=='Leven Seger':
        for alpha in alphas:
            r,e=leven_seger_forcast(data,1,alpha)
            res.append(r)
            err.append(e)
        ind=err.index(min(err))
    elif model=='Vinh':
        for alpha in alphas:
            r,e=vinh_forcast(data,1,alpha)
            res.append(r)
            err.append(e)
        ind=err.index(min(err))
    return res[ind],err[ind],alphas[ind]
        
def train_model_optimized(model,data):
    err=0
    res=0
    if model=='Croston':
        res,err=optimize_croston(data,model)
    elif model=='Croston TSB':
        res,err=optimize_croston_tsb(data)
    elif model=='Syntetos Boylan':
        res,err=optimize_syntetos_boylan(data)
    elif model=='Leven Seger':
        res,err=optimize_leven_seger(data)
    elif model=='Vinh':
        res,err=optimize_vinh(data)
    return res,err

def optimize_croston(data,model,mid=0.5,step=0.05):
    mid=mid
    left=mid-step
    right=mid+step
    f_mid,err_mid=croston_forcast(data,1,mid)
    f_left,err_left=croston_forcast(data,1,left)
    f_right,err_right=croston_forcast(data,1,right)
    while True:
        if err_mid>err_right and err_mid>err_left:
            f_mid,err_mid,mid,l=train_model(data,model)
            #print('Opt Count:{} Iter Count:{}'.format(count,l))
            #count+=l
            break
        elif err_mid<=err_right and err_mid<=err_left:
            break
        elif err_mid<err_right and err_mid>err_left:
            right=mid
            mid=left
            left=left-step
            f_right,err_right=f_mid,err_mid
            f_mid,err_mid=f_left,err_left
            f_left,err_left=croston_forcast(data,1,left)
        elif err_mid>err_right and err_mid<err_left:
            left=mid
            mid=right
            right=right+step
            f_left,err_left=f_mid,err_mid
            f_mid,err_mid=f_right,err_right
            f_right,err_right=croston_forcast(data,1,right)
    return f_mid,err_mid

def optimize_croston_tsb(data,mid=0.5,step=0.05):
    mid=mid
    left=mid-step
    right=mid+step
    f_mid,err_mid=croston_tsb_forcast(data,1,mid)
    f_left,err_left=croston_tsb_forcast(data,1,left)
    f_right,err_right=croston_tsb_forcast(data,1,right)
    while True:
        if err_mid<=err_right and err_mid<=err_left:
            break
        elif err_mid<err_right and err_mid>err_left:
            right=mid
            mid=left
            left=left-step
            f_right,err_right=f_mid,err_mid
            f_mid,err_mid=f_left,err_left
            f_left,err_left=croston_tsb_forcast(data,1,left)
        elif err_mid>err_right and err_mid<err_left:
            left=mid
            mid=right
            right=right+step
            f_left,err_left=f_mid,err_mid
            f_mid,err_mid=f_right,err_right
            f_right,err_right=croston_tsb_forcast(data,1,right)
    return f_mid,err_mid

def optimize_syntetos_boylan(data,mid=0.5,step=0.05):
    mid=mid
    left=mid-step
    right=mid+step
    f_mid,err_mid=syntetos_boylan_forcast(data,1,mid)
    f_left,err_left=syntetos_boylan_forcast(data,1,left)
    f_right,err_right=syntetos_boylan_forcast(data,1,right)
    while True:
        if err_mid<=err_right and err_mid<=err_left:
            break
        elif err_mid<err_right and err_mid>err_left:
            right=mid
            mid=left
            left=left-step
            f_right,err_right=f_mid,err_mid
            f_mid,err_mid=f_left,err_left
            f_left,err_left=syntetos_boylan_forcast(data,1,left)
        elif err_mid>err_right and err_mid<err_left:
            left=mid
            mid=right
            right=right+step
            f_left,err_left=f_mid,err_mid
            f_mid,err_mid=f_right,err_right
            f_right,err_right=syntetos_boylan_forcast(data,1,right)
    return f_mid,err_mid

def optimize_leven_seger(data,mid=0.5,step=0.05):
    mid=mid
    left=mid-step
    right=mid+step
    f_mid,err_mid=leven_seger_forcast(data,1,mid)
    f_left,err_left=leven_seger_forcast(data,1,left)
    f_right,err_right=leven_seger_forcast(data,1,right)
    while True:
        if err_mid<=err_right and err_mid<=err_left:
            break
        elif err_mid<err_right and err_mid>err_left:
            right=mid
            mid=left
            left=left-step
            f_right,err_right=f_mid,err_mid
            f_mid,err_mid=f_left,err_left
            f_left,err_left=leven_seger_forcast(data,1,left)
        elif err_mid>err_right and err_mid<err_left:
            left=mid
            mid=right
            right=right+step
            f_left,err_left=f_mid,err_mid
            f_mid,err_mid=f_right,err_right
            f_right,err_right=leven_seger_forcast(data,1,right)
    return f_mid,err_mid

def optimize_vinh(data,mid=0.5,step=0.05):
    mid=mid
    left=mid-step
    right=mid+step
    f_mid,err_mid=vinh_forcast(data,1,mid)
    f_left,err_left=vinh_forcast(data,1,left)
    f_right,err_right=vinh_forcast(data,1,right)
    while True:
        if err_mid<=err_right and err_mid<=err_left:
            break
        elif err_mid<err_right and err_mid>err_left:
            right=mid
            mid=left
            left=left-step
            f_right,err_right=f_mid,err_mid
            f_mid,err_mid=f_left,err_left
            f_left,err_left=vinh_forcast(data,1,left)
        elif err_mid>err_right and err_mid<err_left:
            left=mid
            mid=right
            right=right+step
            f_left,err_left=f_mid,err_mid
            f_mid,err_mid=f_right,err_right
            f_right,err_right=vinh_forcast(data,1,right)
    return f_mid,err_mid
"""Forcasting Functions ends here"""

def calculate_abc(df):
    df['Cumulative Contribution']=(np.array(df['Total Value'])/np.array(df['Total Value']).sum())*100
    for i in df.index:
        if i==0:
            df.loc[i,'Cumulative Contribution']=df.loc[i,'Cumulative Contribution']
        elif df.loc[i,'Cumulative Contribution']>0:
            df.loc[i,'Cumulative Contribution']=df.loc[i,'Cumulative Contribution']+df.loc[i-1,'Cumulative Contribution']
        else:
            df.loc[i,'Cumulative Contribution']=0
        
    conditions=[(df['Cumulative Contribution']==0),
                (df['Cumulative Contribution']<=settings.abc_dist[0]),
                (df['Cumulative Contribution']>settings.abc_dist[0]) & (df['Cumulative Contribution']<=settings.abc_dist[1]),
                (df['Cumulative Contribution']>settings.abc_dist[1]) & (df['Cumulative Contribution']<=settings.abc_dist[2])]
    values=['D','A','B','C']
    df['ABC']=np.select(conditions,values)


def read_files(path):
    files = [f for f in listdir(path)]
    return files

# custom
def change_filename(username,companyname,brand,val):
    current_datetime = datetime.datetime.now()
    new_filename = '{}_{}_{}_{}_{}_{}_{}_{}.xlsx'.format(
        username,
        companyname,
        brand,
        current_datetime.date(),
        current_datetime.time().hour,
        current_datetime.time().minute,
        current_datetime.time().second,
        val
    )
    return new_filename

def get_target(df,username,companyname,brand,target_days):
    raw = change_filename(username,companyname,brand,"raw")
    tar = change_filename(username,companyname,brand,"target")
    dt=df.copy()
    dt=dt.loc[dt['SOQ Forcasted']!=0]
    dt=dt[['Part No.','Part Name','Part Type','Price','ABC','FMS','Inventory','Back Order','SOQ Forcasted','Forcasted Value']]
    dt.sort_values(by=['FMS','ABC'],axis=0,inplace=True)
    dt.to_excel('{}/{}'.format(settings.reports_path,raw),index=False,sheet_name='Raw')
    # target=int(input('Enter Target or 0:'))
    target  = target_days
    if target == 0:
        pass
    elif target<dt['Forcasted Value'].sum():
        print(target)
        value=dt['Forcasted Value'].tolist()
        s=0
        for i in range(len(value)):
            s+=value[i]
            if s>target:
                dt.iloc[:i].to_excel('{}/{}'.format(settings.reports_path,tar),index=False)
                break
    else:
        residual=target-dt['Forcasted Value'].sum()
        p_sum=dt.loc[(dt['FMS']=='F'),'Price'].sum()
        p_cycle=int(residual/p_sum)
        if p_cycle>0:
            dt.loc[(dt['FMS']=='F'),'SOQ Forcasted']+=p_cycle
            dt.loc[(dt['FMS']=='F'),'Forcasted Value']=dt.loc[(dt['FMS']=='F'),'SOQ Forcasted']*dt.loc[(dt['FMS']=='F'),'Price']
            #residual-=p_sum*p_cycle
            dt.to_excel('{}/{}'.format(settings.reports_path,tar),index=False)


# my own (subhadip)

def select_specific_file(files,username,companyname,brand):
    selected_files = []
    for file in files:
        if file.startswith('{}_{}_{}'.format(username,companyname,brand)):
            selected_files.append(file)
    try:
        return selected_files[-1]
    except:
        return False