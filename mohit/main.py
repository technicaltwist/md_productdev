from . import settings
from . import functions as ft

import shutil
import os
from os import listdir
from .run_forcast import run_forcast

def main(forcast_days,target_days,process_transit_days,username,companyname,brand):

    file = ft.select_specific_file(
        ft.read_files(settings.inbound_path),
        username,
        companyname,
        brand
    )
    shutil.move('{}\{}'.format(settings.inbound_path,file),'{}\{}'.format(settings.outbound_path,file))

    outbound_file= ft.select_specific_file(
        ft.read_files(settings.outbound_path),
        username,
        companyname,
        brand
    )
    path='{}\{}'.format(settings.outbound_path,outbound_file)
    run_forcast(
        path,
        outbound_file,
        forcast_days,
        target_days,
        process_transit_days,
        username,
        companyname,
        brand
    )
    os.remove(path)