import numpy as np
from . import functions as ft
from . import settings
class Forcast():
    storage={}
    def __init__(self,data,inventory,price,backorder,sku_name,name_list,sku_list,category,
        forcast_days,saftey_stock_days,process_transit_days,period):
        self.data=data
        self.sku_name=sku_name
        self.index=sku_list.index(self.sku_name)
        self.name=name_list[self.index]
        self.part_type=category[self.index]
        self.period=period
        self.original_data=self.get_original_data()
        self.ttm_data,self.modified_data=self.modify_data()
        self.total=np.sum(self.original_data)
        self.price=price[self.index]
        self.value=self.price*self.total
        self.max_demand=np.max(self.ttm_data)
        self.min_demand=np.min(self.ttm_data)
        self.mean=np.mean(self.ttm_data)
        self.median=np.median(self.ttm_data)
        self.std=np.std(self.ttm_data)
        self.adi,self.cv2,self.category=self.adi_cv2_category()
        self.fms=self.get_fms()
        self.smoothening_factor=settings.smoothening_factor
        self.croston,self.croston_error,self.a1 = ft.train_model('Croston',self.modified_data)
        self.croston_tsb,self.croston_tsb_error,self.a2 = ft.train_model('Croston TSB',self.modified_data)
        self.syntetos_boylon,self.syntetos_boylon_error,self.a3 = ft.train_model('Syntetos Boylan',self.modified_data)
        self.leven_seger,self.leven_seger_error,self.a4 = ft.train_model('Leven Seger',self.modified_data)
        self.vinh,self.vinh_error,self.a5 = ft.train_model('Vinh',self.modified_data)
        self.seasonal,self.seasonal_flag=ft.seasonal_forcast(self.modified_data,self.period,'m')
        self.moving_average=ft.average_forcast(self.modified_data)
        self.non_moving=self.get_non_moving()
        self.inventory=inventory[self.index]
        self.backorder=backorder[self.index]
        self.inventory_value=self.inventory*self.price
        self.lot_size=int(self.moving_average[1])
        self.saftey_stock_days=saftey_stock_days 
        self.saftey_stock=int((self.moving_average[1]/30)*self.saftey_stock_days)
        self.process_transit_days=process_transit_days #
        self.process_transit=int((self.moving_average[1]/30)*self.process_transit_days)
        self.reorder_point=self.saftey_stock+self.process_transit
        self.monthly_demand=0
        self.model_used='None'
        self.alpha_used=0
        self.soq_conventional=self.get_soq_conventional()
        self.soq_exact=self.get_soq_forcasted_new()
        self.lot_size_new=int((self.monthly_demand/30)*forcast_days)
        self.saftey_stock_new=self.get_saftey_stock()
        self.process_transit_new=int((self.monthly_demand/30)*self.process_transit_days)
        self.reorder_point_new=self.saftey_stock_new+self.process_transit_new
        self.soq_forcasted_new=self.get_soq()
        self.forcasted_value=self.soq_forcasted_new*self.price
        #print(self.moving_average)
        #self.reversion_ratios=[self.soq_forcasted/i if i>0 else 0.0 for i in self.moving_average]
        self.final_output=self.get_final_output()
        Forcast.storage[sku_name]=self
    
    def get_original_data(self):
        return self.data[self.index]
    
    def modify_data(self):
        i=0
        try:
            while True:
                if len(self.original_data)>0 and self.original_data[i]>0:
                    break
                else:
                    i+=1
            temp = self.original_data[i:]
        except:
            return [0],[0]
        else:
            if len(temp)>12:
                return temp[-12:],self.original_data[i:]
            else:
                return temp,temp
    #adi= sum of periods between two consicutive non zero periods
        
        
    def get_items(self,item=0):
        try:
            end=len(self.modified_data)-1
            while True:
                if self.modified_data[end]==0:
                    end-=1
                else:
                    break
        except:
            return []
        else:
            pruned=self.modified_data[:end+1]
            
            non_zero = [i for i in pruned if i != item]
            
            return non_zero

    def get_category(self,adi,cv2):
        if adi<= 1.32 and cv2<=0.49:
            return 'Smooth'
        elif adi> 1.32 and cv2<=0.49:
            return 'Intermittent'
        elif adi<= 1.32 and cv2>0.49:
            return 'Eratic'
        else:
            return 'Lumpy'

    def adi_cv2_category(self):
        non_zero=self.get_items()
        if len(non_zero)>1:
            #q=0
            #d=0
            #for i in self.modified_data:
                #if i==0:
                    #q+=1
                #else:
                    #d+=1
            #adi=q/d
            adi=(len(self.modified_data)-1)/(len(non_zero)-1)
            
            #non zero to be passed
            
            #cv2=(np.sqrt(np.std(self.modified_data))/np.mean(self.modified_data))**2
            cv2=(np.std(non_zero)/np.mean(non_zero))**2
            category=self.get_category(adi,cv2)
            return adi,cv2,category
        else:
            return 'Not Defined','Not Defined','Not Defined'
        
    def get_fms(self):
        if self.adi=='Not Defined':
            return 'Z'
        elif self.adi<1.3:
            return 'F'
        elif self.adi>=1.3 and self.adi<2.0:
            return 'M'
        else:
            return 'S'

    def get_soq_conventional(self):
        total=self.inventory+self.backorder
        if total<self.reorder_point:
            return self.lot_size
        else:
            return int(0)
    def get_soq(self):
        total=self.inventory+self.backorder
        if total>self.process_transit_new+self.saftey_stock_new+self.lot_size_new:
            return int(0)
        elif total-self.process_transit_new<=self.saftey_stock_new:
            return self.lot_size_new+self.saftey_stock_new
        elif total-self.process_transit_new>self.saftey_stock_new:
            return self.lot_size_new
    
    def get_soq_forcasted(self):
        val=0
        total=self.inventory+self.backorder
        lst=[self.croston*self.period,self.croston_tsb*self.period,self.syntetos_boylon*self.period,self.leven_seger*self.period]
        if self.category=='Smooth':
            self.monthly_demand=lst[0]
            self.model_used='Croston'
            if (total-lst[0])<=0:
                val=lst[0]-total
        elif self.category=='Lumpy':
            self.monthly_demand=lst[1]
            self.model_used='Croston TSB'
            if (total-lst[1])<=0:
                val=lst[1]-total
        elif self.category=='Eratic':
            self.monthly_demand=lst[2]
            self.model_used='Syntetos Boylon'
            if(total-lst[2])<=0:
                val=lst[2]-total
        elif self.category=='Intermittent':
            self.monthly_demand=lst[2]
            self.model_used='Syntetos Boylon'
            if (total-lst[2])<=0:
                val=lst[2]-total
        return val

    def get_soq_forcasted_new(self):
        val=0
        total=self.inventory+self.backorder
        lst=[self.croston*self.period,self.croston_tsb*self.period,self.syntetos_boylon*self.period,self.leven_seger*self.period,self.vinh*self.period]
        model=['Croston','Croston TSB','Syntetos Boylon','Leven Seger','Vinh']
        error=[self.croston_error,self.croston_tsb_error,self.syntetos_boylon_error,self.leven_seger_error,self.vinh_error]
        alpha=[self.a1,self.a2,self.a3,self.a3,self.a4]
        index=error.index(min(error))
        self.monthly_demand=lst[index]
        self.model_used=model[index]
        self.alpha_used=alpha[index]
        if (total-self.monthly_demand)<=0:
            val=self.monthly_demand-total
        return val
    
    def get_non_moving(self):
        if self.moving_average[0]>0:
            a='Moving'
        elif self.moving_average[0]==0 and self.moving_average[1]>0:
            a='90 Days'
        elif self.moving_average[1]==0 and self.moving_average[2]>0:
            a='180 Days'
        elif self.moving_average[2]==0 and self.moving_average[3]>0:
            a='270 Days'
        else:
            a='360 Days'
        return a
    
    def get_saftey_stock(self):
        if self.fms=='F':
            return int((self.monthly_demand/30)*self.saftey_stock_days)
        elif self.fms=='M':
            return int((self.monthly_demand/30)*self.saftey_stock_days)
        else:
            return int((self.mean/30)*self.saftey_stock_days)
    
    def get_final_output(self):
        final=[]
        final.append(self.sku_name)
        final.append(self.name)
        final.append(self.part_type)
        final=final+self.original_data
        final.append(self.total)
        final.append(self.price)
        final.append(self.value)
        final.append(self.max_demand)
        final.append(self.min_demand)
        final.append(self.mean)
        final.append(self.median)
        final.append(self.std)
        final.append(self.adi)
        final.append(self.cv2)
        final.append(self.category)
        final.append(self.fms)
        #final.append(self.smoothening_factor)
        final.append(self.croston)
        final.append(self.croston_error)
        #final.append(self.croston_level)
        #final.append(self.croston_prob)
        final.append(self.croston_tsb)
        final.append(self.croston_tsb_error)
        final.append(self.syntetos_boylon)
        final.append(self.syntetos_boylon_error)
        final.append(self.leven_seger)
        final.append(self.leven_seger_error)
        final.append(self.vinh)
        final.append(self.vinh_error)
        final.append(self.seasonal)
        final.append(self.seasonal_flag)
        final.append(self.model_used)
        final.append(self.alpha_used)
        final.append(self.period)
        final.append(self.monthly_demand)
        final=final+self.moving_average
        final.append(self.non_moving)
        #final=final+self.reversion_ratios
        final.append(self.inventory)
        final.append(self.backorder)
        final.append(self.inventory_value)
        final.append(self.lot_size_new)
        final.append(self.saftey_stock_days)
        final.append(self.saftey_stock_new)
        final.append(self.process_transit_days)
        final.append(self.process_transit_new)
        final.append(self.reorder_point_new)
        final.append(self.soq_conventional)
        final.append(self.soq_forcasted_new)
        final.append(self.forcasted_value)
        return final


