#inbound_path = 'D:\metric_drive\inbound'
from softui.settings import BASE_DIR
import os

# inbound_path = 'inbound'
# outbound_path = 'outbound'
# processed_path = 'processed'
# reports_path='reports'
inbound_path = os.path.join(BASE_DIR,'mohit\inbound')
outbound_path = os.path.join(BASE_DIR,'mohit\outbound')
processed_path = os.path.join(BASE_DIR,'mohit\processed')
reports_path = os.path.join(BASE_DIR,'mohit\Reports')

# forcast_days=15 #forecast_period
saftey_stock_days=7 # safety_stock
process_transit_days=10 #lead time

def test(brand,forecast_period,safety_stock,lead_time):
    forcast_  = forecast_period
    return forcast_


smoothening_factor=0.4
abc_dist=[70,90,100]
target = 0

columns_creation=['Total Quantity','Price','Total Value','TTM Max Demand','TTM Min Demand','TTM Mean','TTM Median','TTM Std','ADI','CV2','Category','FMS','Croston','RMSE Croston','Croston TSB','RMSE Croston TSB','Syntotes Bolyan','RMSE Syntotes Bolyan','Leven Seger','RMSE Leven Seger','Vinh','RMSE Vinh','Same Period Last Year','SPLY Flag','Model Used','Alpha','Forcast Period','Period Demand','3 Month MA','6 Month MA','9 Month MA','12 Month MA','Non Moving','Inventory','Back Order','Inventory Value','Lot Size','Saftey Stock Days','Saftey Stock','Processsing Transit Days','Processing Transit','Reorder Point','SOQ Conventional','SOQ Forcasted','Forcasted Value']
columns_output=['Total Quantity','Price','Total Value','Cumulative Contribution',
                'TTM Max Demand','TTM Min Demand','TTM Mean','TTM Median','TTM Std','ADI','CV2','Category','ABC','FMS','Non Moving',
                '3 Month MA','6 Month MA','9 Month MA','12 Month MA',
                'Croston','RMSE Croston','Croston TSB','RMSE Croston TSB','Syntotes Bolyan','RMSE Syntotes Bolyan','Leven Seger','RMSE Leven Seger','Vinh','RMSE Vinh',
                'Same Period Last Year','SPLY Flag','Model Used','Alpha','Forcast Period','Period Demand',
                'Inventory','Back Order','Inventory Value','Lot Size','Saftey Stock Days','Saftey Stock',
                'Processsing Transit Days','Processing Transit','Reorder Point',
                'SOQ Conventional','SOQ Forcasted','Forcasted Value']